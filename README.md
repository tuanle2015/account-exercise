# Question 3: answer and explain the time & space complexity for your solution

## The time complexity of methods in class UserNode
### addChild(int parentId, UserNode childNode)
The time comlexity is O(1) because:
- First it will take c time to call method findUserNode.
- Second it will take c time to call addChild method
- Finally it will take c time to return value 
--> Total time = c + c + c = 3c --> Time complexity = O(1), with c = constant.
--> Total space =O(1)

### printAllUserTreePermission()
The time comlexity is O(N^2*logN) because:
- First it will take N time to loop through of all user node: N time
- Second it will take N-1 time to loop through it's children: N-1 time
- Finally it will takge logN time to call recusive method for children of it's children: logN time
--> Total time N*(N-1)*logN --> Time complexity = O(N*(N-1)*logN) = O(N^2*logN)
--> Total space =O(N^2)

### printCurrentNodePermission(UserNode userNode)
The time comlexity is O(logN) because: 
- First it will take c time to get permission of current Node
- After that it will takge logN time to call recusive method for children of it's children 
to get children permission: logN time
--> Total time c*logN --> Time complexity = O(logN)
--> Total space =O(N)

### addUserPermission(int userId, String permission)
The time comlexity is O(1) because:
- First it will take c time to call method findUserNode to find the parent
- Finally it will take c time to execute add permission
--> Total time = c + c = 2c --> Time complexity = O(1), with c = constant.
--> Total space =O(1)

### removeUserPermission(int userId, String permission)
The time comlexity is O(1) because:
- First it will take c time to call method findUserNode to find the parent
- Finally it will take c time to execute remove permission
--> Total time = c + c = 2c --> Time complexity = O(1), with c = constant.
--> Total space =O(1)

### findUserNode(int userId)
The time comlexity is O(1) because:
- It will take c time to find user in the map, not loop through all map
--> Total time = c --> Time complexity = O(1), with c = constant.
--> Total space =O(1)

