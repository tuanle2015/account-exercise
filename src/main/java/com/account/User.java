package com.account;

import java.util.Set;

public class User {
    private int userId;
    private Set<String> permissions;

    public User(int userId, Set<String> permissions) {
        this.setUserId(userId);
        if (permissions == null || permissions.size() == 0) {
            throw new NullPointerException("The user must have at least 1 permission");
        } else {
            this.permissions = permissions;
        }
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Set<String> getPermissions() {
        return permissions;
    }

    public void setPermissions(Set<String> permissions) {
        this.permissions = permissions;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", permissions=" + permissions +
                '}';
    }
}
