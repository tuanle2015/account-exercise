package com.account;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class ReadDataFromFile {

    private static final String DELIMETER = " ";
    private static final String CEO = "CEO";
    private static int NUMBER_USER_INPUT = 1;
    private static int LINE_ROOT_PERMISSION = 2;
    private static int LINE_USER_PERMISSION_START = 3;

    /**
     * Read data from file and create UserNode Tree
     * @param fileInput
     * @return
     */
    public UserNode createUsersTreeFromFile(String fileInput) {
        UserNode rootNode = null;
        try {
            File file = new File(getClass().getClassLoader().getResource(fileInput).getFile());
            Scanner sc = new Scanner(file);
            int numberUsersInput = 0;
            int numberOfLines = 0;
            int lineForSetParentStart = 0;
            int lineForSetParentEnd = 0;
            int userIndex = 1;
            List<User> userLists = new LinkedList<>();
            while (sc.hasNextLine()) {
                numberOfLines++;
                String[] lineData = sc.nextLine().split(DELIMETER);
                if (numberOfLines == NUMBER_USER_INPUT) {
                    numberUsersInput = (Integer.parseInt(lineData[0]));
                    lineForSetParentStart = numberUsersInput + 3;
                    lineForSetParentEnd = numberUsersInput * 2 + 2;
                    continue;
                }
                if (numberOfLines == LINE_ROOT_PERMISSION) {
                    // create root user and root node
                    User userRoot = new User(99999, Arrays.stream(lineData).collect(Collectors.toSet()));
                    userLists.add(userRoot);
                    rootNode = new UserNode(userRoot);
                    continue;
                }
                if (numberOfLines >= LINE_USER_PERMISSION_START && numberOfLines <= numberUsersInput + 2) {
                    User user = new User(numberOfLines - 2, Arrays.stream(lineData).collect(Collectors.toSet()));
                    userLists.add(user);
                    continue;
                }
                if (numberOfLines >= lineForSetParentStart && numberOfLines <= lineForSetParentEnd) {
                    UserNode userNode = new UserNode(userLists.get(userIndex));
                    User userParent = lineData[0].equals(CEO) ? userLists.get(0) : userLists.get((Integer.parseInt(lineData[0])));
                    rootNode.addChild(userParent.getUserId(), userNode);
                    userIndex++;
                }
            }
            if (numberOfLines < numberUsersInput * 2 + 2) {
                System.out.println("Missing data in file. User don't have permission or parent");
                return null;
            }
        } catch (NullPointerException ex) {
            throw new NullPointerException("File data isn't found");
        } catch (NumberFormatException ex) {
            throw new NumberFormatException("Value in file input is not valid");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return rootNode;
    }

    /**
     * Get query command from file
     * @param fileQuery
     * @return
     */
    public List<String[]> getQueryFromFile(String fileQuery) {
        List<String[]> commandsFromFile = new LinkedList<>();
        File file = new File(getClass().getClassLoader().getResource(fileQuery).getFile());
        try {
            Scanner sc = new Scanner(file);
            while (sc.hasNextLine()) {
                String[] lineData = sc.nextLine().split(DELIMETER);
                if (lineData[0].contains("ADD") || lineData[0].contains("QUERY") || lineData[0].contains("REMOVE")) {
                    commandsFromFile.add(lineData);
                } else {
                    throw new Exception("Query command is invalid");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return commandsFromFile;
    }
}
