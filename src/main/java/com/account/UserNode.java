package com.account;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class UserNode {
    private User data;
    private List<UserNode> children = new ArrayList();
    private HashMap<Integer, UserNode> locattion = new LinkedHashMap<Integer, UserNode>();

    public UserNode(User data) {
        this.setData(data);
        this.getLocattion().put(data.getUserId(), this);
    }

    /**
     * Add child Node to parent node
     * @param parentId
     * @param childNode
     * @return
     */
    public UserNode addChild(int parentId, UserNode childNode) {
        UserNode parentNode = findUserNode(parentId);
        parentNode.addChild(childNode);
        return parentNode;
    }

    private UserNode addChild(UserNode childNode) {
        this.getChildren().add(childNode);
        this.getLocattion().put(childNode.getData().getUserId(), childNode);
        childNode.locattion = this.locattion;
        return this;
    }

    /**
     * Print permission of all node and their children
     */
    public void printAllUserTreePermission() {
        Map<Integer, UserNode> mapUserNode = this.getLocattion();
        for (UserNode userNode : mapUserNode.values()) {
            printCurrentNodePermission(userNode);
        }
    }

    private static final String BREAK_LINE = "\n";

    /**
     * Print permission of current node
     * @param userNode
     */
    public void printCurrentNodePermission(UserNode userNode) {
        // Get current node permission
        Set<String> currentPermission = new HashSet<>();
        currentPermission.addAll(userNode.getData().getPermissions());
        // Get all permission of current node include its children
        Set<String> allPermission = getChildrenPermission(userNode.getChildren(), currentPermission);
        System.out.print(String.join(",", allPermission) + BREAK_LINE);
    }

    private Set<String> getChildrenPermission(List<UserNode> children, Set<String> currentPermission) {
        for (UserNode child : children) {
            Set<String> childPermissions = child.getData().getPermissions();
            currentPermission.addAll(childPermissions);
            if (child.getChildren().size() > 0) {
                child.getChildrenPermission(child.getChildren(), currentPermission);
            }
        }
        return currentPermission;
    }

    /**
     * Add permission to UserNode
     * @param userId
     * @param permission
     */
    public void addUserPermission(int userId, String permission) {
        UserNode updateUser = findUserNode(userId);
        updateUser.getData().getPermissions().add(permission);
    }

    /**
     * Remove permission from UserNode
     * @param userId
     * @param permission
     */
    public void removeUserPermission(int userId, String permission) {
        UserNode user = findUserNode(userId);
        user.getData().getPermissions().remove(permission);
    }

    public UserNode findUserNode(int userId) {
        return this.getLocattion().get(userId);
    }

    public User getData() {
        return data;
    }

    public void setData(User data) {
        this.data = data;
    }

    public List<UserNode> getChildren() {
        return children;
    }

    public void setChildren(List<UserNode> children) {
        this.children = children;
    }

    public HashMap<Integer, UserNode> getLocattion() {
        return locattion;
    }

    public void setLocattion(HashMap<Integer, UserNode> locattion) {
        this.locattion = locattion;
    }
}
