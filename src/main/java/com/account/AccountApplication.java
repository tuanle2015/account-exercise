package com.account;

import java.util.List;

public class AccountApplication {

    private static String fileInput = "input-data.txt";
    private static String fileQuery = "query-data.txt";

    public static void main(String[] args) {
        ReadDataFromFile readDataFromFile = new ReadDataFromFile();
        //========== ANSWER 1 ==========
        UserNode userNodeTree = readDataFromFile.createUsersTreeFromFile(fileInput);
        userNodeTree.printAllUserTreePermission();
        //========== ANSWER 4 ==========
        updateUserNodeByQuery(readDataFromFile, userNodeTree);

    }

    private static void updateUserNodeByQuery( ReadDataFromFile readDataFromFile,UserNode userNodeTree){
        List<String[]> commandUpdateData = readDataFromFile.getQueryFromFile(fileQuery);
        for (String[] commandData : commandUpdateData) {
            String query = commandData[0];
            if (query.equalsIgnoreCase("ADD")) {
                userNodeTree.addUserPermission(Integer.parseInt(commandData[1]), commandData[2]);
                continue;
            }
            if (query.equalsIgnoreCase("QUERY")) {
                String userInput = commandData[1];
                UserNode userUpdate = userNodeTree.findUserNode(userInput.equals("CEO") ? 99999 : Integer.parseInt(userInput));
                userUpdate.printCurrentNodePermission(userUpdate);
                continue;
            }
            if (query.equalsIgnoreCase("REMOVE")) {
                userNodeTree.removeUserPermission(Integer.parseInt(commandData[1]), commandData[2]);
                continue;
            }
        }

    }

}
