package com.account;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;

public class PrintUserNodePermissionTest {
    private final InputStream systemIn = System.in;
    private final PrintStream systemOut = System.out;

    private ByteArrayInputStream testIn;
    private ByteArrayOutputStream testOut;

    @Before
    public void setUpOutput() {
        testOut = new ByteArrayOutputStream();
        System.setOut(new PrintStream(testOut));
    }

    private String getOutput() {
        return testOut.toString();
    }

    @After
    public void restoreSystemInputOutput() {
        System.setIn(systemIn);
        System.setOut(systemOut);
    }

    @Test
    public void whenCallPrintAllUserTreePermission_ThenReturnAllPermissionOfAllUser() {
        UserNode root = new UserNode(new User(99999, Stream.of("A", "F").collect(Collectors.toSet())));
        User user1 = new User(1, Stream.of("A", "B").collect(Collectors.toSet()));
        UserNode userNode1 = new UserNode(user1);
        User user2 = new User(2, Stream.of("A", "C", "E").collect(Collectors.toSet()));
        UserNode userNode2 = new UserNode(user2);
        root.addChild(99999, userNode1);
        root.addChild(99999, userNode2);

        root.printAllUserTreePermission();
        final String expectedOutput = "A,B,C,E,F\nA,B\nA,C,E\n";
        assertEquals(expectedOutput, getOutput());
    }

    @Test
    public void whenCallPrintCurrentNodePermission_ThenReturnCurrrentNodePermission() {
        UserNode root = new UserNode(new User(99999, Stream.of("A", "F").collect(Collectors.toSet())));
        User user1 = new User(1, Stream.of("A", "B").collect(Collectors.toSet()));
        UserNode userNode1 = new UserNode(user1);
        User user2 = new User(2, Stream.of("A", "C", "E").collect(Collectors.toSet()));
        UserNode userNode2 = new UserNode(user2);
        root.addChild(99999, userNode1);
        root.addChild(99999, userNode2);

        root.printCurrentNodePermission(userNode2);
        final String expectedOutput = "A,C,E\n";
        assertEquals(expectedOutput, getOutput());
    }
}
