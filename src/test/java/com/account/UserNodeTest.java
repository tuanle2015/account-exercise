package com.account;

import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;

public class UserNodeTest {
    UserNode root = new UserNode(new User(99999, Stream.of("A", "F").collect(Collectors.toSet())));

    @Test
    public void whenCreateUserNode_thenHaveUserData() {
        Set<String> permissions = new HashSet<String>();
        permissions.add("A");
        permissions.add("B");
        permissions.add("C");
        User user = new User(1, permissions);
        UserNode userNode = new UserNode(user);
        assertEquals(user, userNode.getData());
        assertEquals(1, userNode.getData().getUserId());
    }

    @Test
    public void whenCreateUserNode_ThenUserHaveAtLeast1Permission() {
        Set<String> permissions = Collections.singleton("A");
        User user = new User(1, permissions);
        assertEquals(1, user.getPermissions().size());
    }

    @Test(expected = NullPointerException.class)
    public void whenUserHaveNotPermission_ThenThrowException() {
        User user = new User(1, null);
        assertEquals(1, user.getPermissions().size());
    }

    @Test
    public void whenAddUserToParent_thenParentNodeHaveUserNode() {
        User user = new User(1, Collections.singleton("B"));
        UserNode childNode = new UserNode(user);
        UserNode parentNode = root.addChild(99999, childNode);
        Assert.assertTrue(parentNode.getChildren().contains(childNode));
    }

    @Test
    public void whenParentNodeHaveChildren_ThenParentHavePermissionOfChildren() {
        User user1 = new User(1, Stream.of("A", "B").collect(Collectors.toSet()));
        UserNode userNode1 = new UserNode(user1);
        User user2 = new User(2, Stream.of("A", "C", "E").collect(Collectors.toSet()));
        UserNode userNode2 = new UserNode(user2);
        root.addChild(99999, userNode1);
        root.addChild(99999, userNode2);

        User user3 = new User(3, Stream.of("A").collect(Collectors.toSet()));
        UserNode userNode3 = new UserNode(user3);
        root.addChild(1, userNode3);
        Set<String> rootPermission = root.getData().getPermissions();
        Set<String> userNode1Permission = userNode1.getData().getPermissions();
        Set<String> userNode2Permission = userNode2.getData().getPermissions();
        Set<String> userNode3Permission = userNode3.getData().getPermissions();
        Set<String> actualPermission = new HashSet<>();
        actualPermission.addAll(rootPermission);
        actualPermission.addAll(userNode1Permission);
        actualPermission.addAll(userNode2Permission);
        actualPermission.addAll(userNode3Permission);
        Assert.assertEquals("[A, B, C, E, F]", actualPermission.toString());
    }

    @Test
    public void whenAdd5UserToRoot_thenReturnUserNodeTree() {
        User user1 = new User(1, Stream.of("A", "B").collect(Collectors.toSet()));
        UserNode userNode1 = new UserNode(user1);
        User user2 = new User(2, Stream.of("A", "C", "E").collect(Collectors.toSet()));
        UserNode userNode2 = new UserNode(user2);
        root.addChild(99999, userNode1);
        root.addChild(99999, userNode2);

        User user3 = new User(3, Stream.of("A").collect(Collectors.toSet()));
        UserNode userNode3 = new UserNode(user3);
        User user4 = new User(4, Stream.of("D").collect(Collectors.toSet()));
        UserNode userNode4 = new UserNode(user4);
        User user5 = new User(5, Stream.of("A", "C").collect(Collectors.toSet()));
        UserNode userNode5 = new UserNode(user5);
        root.addChild(1, userNode3);
        root.addChild(1, userNode4);
        root.addChild(1, userNode5);

        List<UserNode> rootTree = root.getChildren();
        assertEquals(2, rootTree.size());
        assertEquals(3, rootTree.get(0).getChildren().size());
        assertEquals(0, rootTree.get(1).getChildren().size());
    }

    @Test
    public void whenCallAddPermissionMethod_thenPermissionUserUpdated() {
        User user1 = new User(1, Stream.of("A", "B").collect(Collectors.toSet()));
        UserNode userNode1 = new UserNode(user1);
        User user2 = new User(2, Stream.of("A", "C", "E").collect(Collectors.toSet()));
        UserNode userNode2 = new UserNode(user2);
        root.addChild(99999, userNode1);
        root.addChild(99999, userNode2);

        User user3 = new User(3, Stream.of("A").collect(Collectors.toSet()));
        UserNode userNode3 = new UserNode(user3);
        root.addChild(1, userNode3);

        root.addUserPermission(2, "X");
        List<UserNode> children = root.getChildren();
        Assert.assertTrue(!children.get(0).getData().getPermissions().contains("X"));
        Assert.assertTrue(children.get(1).getData().getPermissions().contains("X"));
    }

    @Test
    public void whenCallRemovePermissionMethod_thenPermissionUserRemoved() {
        User user1 = new User(1, Stream.of("A", "B").collect(Collectors.toSet()));
        UserNode userNode1 = new UserNode(user1);
        User user2 = new User(2, Stream.of("A", "C", "E", "X").collect(Collectors.toSet()));
        UserNode userNode2 = new UserNode(user2);
        root.addChild(99999, userNode1);
        root.addChild(99999, userNode2);

        User user3 = new User(3, Stream.of("A").collect(Collectors.toSet()));
        UserNode userNode3 = new UserNode(user3);
        root.addChild(1, userNode3);

        root.removeUserPermission(2, "X");
        List<UserNode> children = root.getChildren();
        Assert.assertTrue(!children.get(0).getData().getPermissions().contains("X"));
        Assert.assertTrue(!children.get(1).getData().getPermissions().contains("X"));
    }

    @Test
    public void whenCallAddAndRemovePermission_thenPermissionAddedAndRemove() {
        User user1 = new User(1, Stream.of("A", "B").collect(Collectors.toSet()));
        UserNode userNode1 = new UserNode(user1);
        User user2 = new User(2, Stream.of("A", "C", "E").collect(Collectors.toSet()));
        UserNode userNode2 = new UserNode(user2);
        root.addChild(99999, userNode1);
        root.addChild(99999, userNode2);

        User user3 = new User(3, Stream.of("A").collect(Collectors.toSet()));
        UserNode userNode3 = new UserNode(user3);
        root.addChild(1, userNode3);

        root.addUserPermission(2, "X");
        root.removeUserPermission(2, "E");
        List<UserNode> children = root.getChildren();
        Assert.assertTrue(children.get(1).getData().getPermissions().contains("X"));
        Assert.assertTrue(!children.get(1).getData().getPermissions().contains("E"));
    }
}
