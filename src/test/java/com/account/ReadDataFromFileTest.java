package com.account;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;


public class ReadDataFromFileTest {

    private static String fileInput = "input-valid-data.txt";
    private static String fileInput1 = "input-valid-data1.txt";

    private static String fileInvalid = "input-invalid-data.txt";
    private static String fileInvalid1 = "input-invalid-data1.txt";
    private static String fileInvalid2 = "input-invalid-data2.txt";
    private static String fileInvalid3 = "input-invalid-data3.txt";

    private static String queryValid = "query-valid.txt";

    ReadDataFromFile readDataFromFile = new ReadDataFromFile();

    @Test
    public void whenReadData_thenReturnUserNodeTree() {
        UserNode userNodeTree = readDataFromFile.createUsersTreeFromFile(fileInput);
        UserNode user1 = userNodeTree.getChildren().get(0);
        UserNode user2 = userNodeTree.getChildren().get(1);
        Assert.assertEquals(99999, userNodeTree.getData().getUserId());
        Assert.assertEquals(user1, userNodeTree.getLocattion().get(1));
        Assert.assertEquals(user2, userNodeTree.getLocattion().get(2));
    }

    @Test(expected = NullPointerException.class)
    public void whenFileNameNotFound_thenThrowNullPointerException() {
        UserNode userNodeTree = readDataFromFile.createUsersTreeFromFile("fileNotFound");
    }

    @Test
    public void whenFileInputIsNotValid_thenReturnNull() {
        UserNode userNodeTree = readDataFromFile.createUsersTreeFromFile(fileInvalid);
        Assert.assertEquals(null, userNodeTree);
    }

    @Test(expected = Exception.class)
    public void whenFirstLineIsNotNumber_thenReturnException() {
        UserNode userNodeTree = readDataFromFile.createUsersTreeFromFile(fileInvalid1);
    }

    @Test(expected = Exception.class)
    public void whenSetParentNotCEOIsStringthenReturnException() {
        UserNode userNodeTree = readDataFromFile.createUsersTreeFromFile(fileInvalid2);
    }

    @Test
    public void whenReadQueryInFile_thenReturnListQuery() {
        List<String[]> listQuery = readDataFromFile.getQueryFromFile(queryValid);
        Assert.assertEquals("ADD", listQuery.get(0)[0]);
        Assert.assertEquals("2", listQuery.get(0)[1]);
        Assert.assertEquals("X", listQuery.get(0)[2]);
    }
}
